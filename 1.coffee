express = require "express"
app = express()
app.get "/", (req, res) ->
  filename = req.query.filename
  res.cookie 'referer', req.header('Referer')
  res.type 'application/octet-stream'
  res.set "Content-Disposition", "attachment; filename=\"#{filename}\""
  res.sendfile __dirname+'/file.doc'

server = app.listen(3000)