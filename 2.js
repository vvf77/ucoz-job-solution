"use strict";

var testArray = [];
var maxRnd = (1<<16)*(1<<16);
var arrSize = 1000000;
var filling_unique_1 = function(){
	var nextRnd, existing = {
		none:true
	};
    for(var i =0;i<arrSize-1;i++){
		nextRnd='none';
		while(existing[nextRnd])
			nextRnd = Math.random() * maxRnd;
		existing[nextRnd] = i;
		testArray[i] = nextRnd;
    }
};
var filling_unique_2 = function(){
	var nextRnd, existing = {
		none:true
	};
    for(var i =0;i<arrSize-1;i++){
		nextRnd='none';
		while(existing[nextRnd])
			nextRnd = Math.random() * maxRnd;
		existing[nextRnd] = i; 
    }
    delete existing.none;
    testArray = Object.keys( existing ).map( function(x){ return 1*x; });
};

var run_filling = function( filling_fn ){
	filling_fn();
	var doubleValue = testArray[ Math.floor( Math.random()*testArray.length ) ];

    testArray.push( doubleValue );
    console.log('    set double value =', doubleValue);
};

var get_double_value_1 = function( arr ){
	arr.sort();
	for(var i=1,l=arr.length;i<l;i++){
		if( arr[i-1] == arr[i]){
			return [arr[i],i,0];
		}
	}
	return ['none',-1,-1];
};

var get_double_value_2 = function( arr ){
	var j,cacheHash = {};
	for(var i=1,l=arr.length;i<l;i++){
		j=cacheHash[ arr[i] ]
		if( j ){
			//console.log( 'double at ', i, j, arr[i]);
			return [arr[i], i,j];
		}
		cacheHash[arr[i]] = i;
	}
	console.log( ' something wrong: ', cacheHash);
	return ['none',-1,-1];
};

var testingCases = {
	fill1_get1: [ filling_unique_1, get_double_value_1],
	fill1_get2: [ filling_unique_1, get_double_value_2],
	fill2_get1: [ filling_unique_2, get_double_value_1],
	fill2_get2: [ filling_unique_2, get_double_value_2],
};

var do_test_case_one = function(name){
	//console.log('>>>>> start to do', name );
	var beginTime = (new Date()).getTime();
	testArray = [];
	run_filling( testingCases[name][0] );
	//console.log(' ==', testArray.join(' '));
	var doubleValue = testingCases[ name ][1]( testArray )[0];
	var runTime = (new Date()).getTime()- beginTime;
	//console.log('<<<<< done', name );
	//console.log('\ttime=', runTime );
	console.log('    get double value =', doubleValue);
	return runTime;
};
var loopsPerTest = 10;
var do_test_case = function(name){
	var i, sum=0;
	console.log('>>>>> start to do', name );

	for(i=0 ; i<loopsPerTest ; i++){
		sum += do_test_case_one(name);
	}
	console.log('<<<<< done', name );
	console.log('\taverage time=', sum/loopsPerTest );
};

var results={};
for(var tc in testingCases)if(testingCases.hasOwnProperty(tc)){
	do_test_case(tc);
}