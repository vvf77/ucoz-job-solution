# 3. Написать скрипт, который реализует:
	# а) форму добавления в базу данных клиентов (телефон + email)
	# б) форму восстановления номера телефона по указанному email, причём номер телефона должен высылаться на этот email
	# в) организовать защиту данных таким образом, чтобы при получении
	# злоумышленником доступа к САЙТУ (к базе данных клиентов и вашему
	# скрипту), он не смог изъять из нее емейлы и телефоны клиентов
	# г) оформить интерефейс скрипта стилями (css)

express = require "express"
emailjs   = require 'emailjs'
crypto = require 'crypto'
pg = require 'pg'
bodyParser = require "body-parser"

encoder=null
decoder=null

encode =(plainText)->
  return plainText
  # TODO: catch exception if password as wrong
  crypted = encoder.update(''+plainText,'utf8','base64')
  crypted += encoder.final('base64')
  crypted

decode =(crypted)->
  return crypted
  # TODO: catch exception if password as wrong
  uncrypted = decoder.update(''+crypted,'base64','utf8')
  uncrypted += encoder.final('utf8')
  uncrypted

db_request = (sql, next)->
  conString = "postgres://test:test@localhost/test";
  return next('no encoder') unless encoder
  return next('no decoder') unless decoder
  client = new pg.Client(conString)
  client.connect (err)->
    next(err) if err
    client.query sql, next

db_save = (email, phone, next)->
  console.log 'UN encripted:', typeof email, typeof phone
  email = encode email
  phone = encode phone
  console.log 'encripted:', email, phone
  db_request "INSERT INTO emails (email,phone) VALUES ('#{email}','#{phone}')", next

db_get_phone = (email, next)->
  email = encode email
  db_request "SELECT phone FROM emails WHERE email='#{email}' LIMIT 1", (err, result)->
    return next(err) if err
    console.log result
    next null, result[0] && decode(result[0].phone)

initCrypting = (method, passwd)->
  encoder = crypto.createCipher method, passwd
  decoder = crypto.createDecipher method, passwd
#  decoder.setAuthTag(encoder.getAuthTag())

send_email = (email, phone, next)->
  mailer = emailjs.server.connect
    user: 'ucozcadidate@yandex.ru'
    password: '1q2w3e4r.'
    host:smtp.yandex.ru
    ssl: true
  mailer.send
    text: 'you phone number: '+ phone
    to: email
    subject: 'uCoz-test: restore phone'
  , next || ()->

app = express()
# toffee = require 'toffee'
# app.engine 'html', toffee.__express

app.disable 'x-powered-by'

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.post '/save/', (req, res, next)->
  # TODO: make email checking
  return res.send 'Не указан e-mail' unless req.body.email
  return res.send 'Не указан phone' unless req.body.phone
  db_save req.body.email, req.body.phone, (err, result)->
    return res.send "Error: #{err}" if err

    res.send 'Добавлено'

app.post '/restore/', (req, res, next)->
  console.log req.body, typeof req.body
  return res.send 'Не указан e-mail' unless req.body.email
  # TODO: make email checking
  db_get_phone req.body.email, (err, result)->
    return res.send "Error: #{err}" if err
    if result
      send_email req.body.email, result, (err)->
        return res.send "Error: #{err}" if err
        res.send "Напоминание выслано на #{req.body.email}"
    else
      # by security reason here should return valid message like above
      res.send "Нет такого е-майла  #{req.body.email}"


app.use(express.static( __dirname + '/static'))


args=process.argv.slice()
while args[0] in ['node','nodejs','coffee']
  args.shift()

process.exit 99 if args.length < 2 or not args[1]
initCrypting 'aes256', args[1]
#for cm in crypto.getCiphers()
#  initCrypting cm, args[1]
#  randomText = 'TODO: here must be random text'
#  try
#    console.log 'success:', cm, decode( encode(randomText) ) == randomText
#  catch e
#    console.log 'invalid', cm, e

server = app.listen(3000)