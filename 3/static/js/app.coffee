
app = angular.module "test3", []
.controller 'ctrl', ($scope,$http,$q, $timeout)->
  $scope.mode='reg'
  $scope.reg=
    email:''
    phone:''
  $scope.restore =
    email: ''

  $scope.requestPrecessing = false
  aborter = null
  window.scope = $scope # for debug purposes
  makeHttpCall=(url, params)->
    aborter = $q.defer()
    aborter.resolve() if aborter
    $scope.requestPrecessing = true
    httpPromise = $http.post url, params
#      timeout: aborter.promise
    .success ( data )->
      # show message about success
      $scope.message = data
      $timeout ()->
        $scope.message = null
      , 60000
    .then ()->
      $scope.requestPrecessing = false
      aborter = null

  $scope.doReg = ()->
    # TODO: check email exists (if it exists - it is seems like email)
    unless $scope.reg.email
      $scope.message='Invalid or empty email'
      return
    unless $scope.reg.phone
      $scope.message='Empty phone'
      return
    params=
      email:$scope.reg.email
      phone:$scope.reg.phone

    makeHttpCall '/save/', params
    $scope.reg=
      email:''
      phone:''

  $scope.doRestore = ()->
    # TODO: check email exists (if it exists - it is seems like email)
    unless $scope.restore.email
      $scope.message='Invalid or empty email'
      return
    makeHttpCall '/restore/',
      email: $scope.restore.email

    $scope.restore.email = ''
